Software Downloads
------------------------
1. Visual Studio 2019 community edition
2. .NET core 3.1
3. SQL Server 2019 express edition
4. SQL Server Studio Management System.
5. Download the project.

Running the Project
------------------------
1. Install Visual Studio 2019
2. Run Visual Studio
3. Open folder
4. select the project folder and open it
5. Execute the project.

LICENSE
------------------------
I choose MIT License because it allows me to share my code under a copyleft license without forcing others to expose their proprietary code, it's business friendly and open source friendly while still allowing for monetization.
